# **IMEDIA24 SHOP**

## Overview

The project is a CRUD **(Create, Read, Update, Delete)** application that allows users to manage product information. This documentation provides an overview of the application's functionality, architecture, and usage instructions.

## **Table of Contents**


    Setup and Configuration
    Usage
    API Reference

## Setup and Configuration

To set up the project locally, follow these steps:

    - Clone the project repository from [repository URL].
    - Install [required software and dependencies], such as Java JDK [version] and Maven [version].
    - Configure the database connection in the application.properties file.
    - Build the project using Maven: mvn clean install.
    - Run the application: mvn spring-boot:run.

## Usage

    Access the application through [URL or localhost:port] in a web browser.(default is http://localhost:8080/products)
    Create a new product by providing the required information.
    View, update, or delete existing products from the list.
    


## API Reference

The application provides RESTful APIs for performing CRUD operations on products. Here are the main API endpoints:

    GET /products: Retrieves a list of all products.
    GET products?skus=1,2: Retrieves product with sku=1 and sku=2.
    GET /products/{sku}: Retrieves details of a specific product.
    POST /products: Creates a new product.
    PUT /products/{sku}: Updates an existing product.
    DELETE /products/{sku}: Deletes a product.

For more detailed API documentation, you can use Swagger.

    1- Start the application locally.
    2- Open a web browser and navigate to the Swagger UI URL. Typically, it is http://localhost:<port>/swagger-ui, where <port> is the port number on which your application is running.
    3- The Swagger UI will be displayed, showing a list of available endpoints and their corresponding documentation.
    4- Explore the API documentation to understand the request and response structures, available operations, and any additional details provided by Swagger.