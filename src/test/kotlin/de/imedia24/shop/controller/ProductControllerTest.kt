package de.imedia24.shop.controller

import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.service.ProductService
import net.bytebuddy.matcher.ElementMatchers.`is`
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.math.BigDecimal


@ExtendWith(MockitoExtension::class)
@WebMvcTest(ProductController::class)
class ProductControllerTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @MockBean
    private lateinit var service: ProductService

    @Autowired
    private lateinit var productController: ProductController

    @Test
    fun `should update the resource`() {
        val sku = "1"
        val updatedProduct = ProductResponse(sku, "lotfi", "updated description", BigDecimal(11), 1)
        val product = ProductResponse(sku, "lotfi", "old description", BigDecimal(11), 1)

        `when`(service.updateProduct(sku, product)).thenReturn(updatedProduct)

        val updateProduct = service.updateProduct(sku, product)
        mockMvc.perform(
            MockMvcRequestBuilders.put("/products/{sku}", sku)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"sku\": 1, \"name\": \"lotfi\", \"description\": \"old description\", \"price\": 11, \"quantity\": 1}")
        )
            .andExpect(status().isOk)
            .andDo(MockMvcResultHandlers.print())
            .andExpect(jsonPath("$.sku").value(sku))
            .andExpect(jsonPath("$.name").value("lotfi"))
            .andExpect(jsonPath("$.description").value("updated description"))
            .andExpect(jsonPath("$.price").value(11))

        verify(service, times(2)).updateProduct(sku, product)
        verifyNoMoreInteractions(service)
    }

    @Test
    fun testGetProductsBySkuShouldPass() {
        val skus = "1,2,3"
        val expectedProductDetails = listOf(
            ProductResponse("1", "camera", "sony camera", BigDecimal(100), 1),
            ProductResponse("2", "pc", "dell latitude 151", BigDecimal(150), 1),
            ProductResponse("3", "hard drive", "hdd hard drive", BigDecimal(10), 2)
        )

        `when`(service.getProductDetailsBySkus(skus)).thenReturn(expectedProductDetails)

        val responseEntity: ResponseEntity<List<ProductResponse>> = productController.getProductDetailsBySkus(skus)

        assert(responseEntity.statusCode == HttpStatus.OK)
        assert(responseEntity.body == expectedProductDetails)
    }
}
