package de.imedia24.shop.exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus


@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Product not Found")
class ProductNotFoundException(sku: String) : RuntimeException("Resource not found with sku:$sku") {
}

