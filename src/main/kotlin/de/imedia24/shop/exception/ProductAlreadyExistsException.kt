package de.imedia24.shop.exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(value = HttpStatus.CONFLICT, reason = "Product Already Exists")
class ProductAlreadyExistsException(productId: String) : RuntimeException("Product already exists with id $productId")