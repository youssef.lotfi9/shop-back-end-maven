package de.imedia24.shop.mapper

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.domain.product.ProductResponse
import org.springframework.stereotype.Component

@Component
class ProductMapper {

    fun productToResp(productEntity: ProductEntity?): ProductResponse? {
        if (productEntity == null) {
            return null
        }

        return ProductResponse(
            sku = productEntity.sku,
            name = productEntity.name,
            description = productEntity.description ?: "",
            price = productEntity.price,
            quantity =  productEntity.quantity
        )
    }

    fun productsToResp(productEntityList: Iterable<ProductEntity>?): List<ProductResponse>? {
        if (productEntityList == null) {
            return null
        }

        val list = ArrayList<ProductResponse>()
        for (productEntity in productEntityList) {
            productToResp(productEntity)?.let { list.add(it) }
        }

        return list
    }
}