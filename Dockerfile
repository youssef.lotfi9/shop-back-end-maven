FROM openjdk:8-jdk-alpine
COPY target/shop-0.0.1-SNAPSHOT.jar shop.jar
CMD ["java","-jar","shop.jar"]